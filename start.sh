#!/bin/sh
set -e
echo "DomeCloud OpenResty..."
/opt/lixen/nginx/sbin/nginx
/bin/prometheus -config.file=/etc/prometheus/prometheus.yml -storage.local.path=/prometheus -web.console.libraries=/etc/prometheus/console_libraries -web.console.templates=/etc/prometheus/consoles


tail -f /dev/null
